'''
Created on 3. 4. 2015

@author: camlost <jiri.novak@gmail.com>
'''

import datetime
import glob
import io
import os.path
import re

from postfixloglib.loglinedata import LogLineData

class LogParser:
	'''
	classdocs
	'''

	def __init__(self):
		'''
		Constructor
		'''
		
	def __getLogFiles(self, log_path, log_file, start_date, end_date = None):
		'''
		Get list of files located in specified directory whose name is based on specified filename
		which were created between specified dates.
		
		Parameters:
			log_path: Path where log files are located. Typically "/var/log" or "/var/log/mail".
			log_file: Filename of last log. Typically "mail.log". 
			start_date: [YYYY-MM-DD or YYYYMMDD] First day to look for.
			end_date: [YYYY-MM-DD or YYYYMMDD] Last day to look for.
			fn_callback: Function which will be called on each line. LogLineData 
			             object is being passed as the argument.
		
		Return:
			List of files matching the parameters (directory / folder, filename, time).
		'''

		date_format = "%Y%m%d" if start_date.find("-") == -1 else "%Y-%m-%d"
		today = datetime.date.today().strftime(date_format)
		if end_date == None: end_date = today
		
		mask = "" if start_date == today else "*"
		file_filter = "{0}{1}".format(os.path.join(log_path, log_file), mask)
		lst = sorted(glob.glob(file_filter))

		result = []
		if mask != "":
			rxp = re.compile(r"\d{8}")
			for filename in lst:
				match = rxp.search(filename)
				if match:
					date = match.group(0)
					if start_date <= date and date <= end_date:
						result.append(filename)
						
			if end_date == today:
				file_list = glob.glob(os.path.join(log_path, log_file))
				result = result + sorted(file_list)
		else:
			result = lst
		return result
	
	def parseFile(self, fp, fn_callback = None):
		'''
		Parse file and call function after each line if specified.
		
		Parameters:
			fp: File pointer.
			fn_callback: Function which will be called on each line. LogLineData 
			             object is being passed as the argument.
		'''
		
		for line in fp:
			data = LogLineData(line)
			fn_callback(data)
	
	def parseLog(self, log_path, log_file_base, start_date, end_date, fn_callback = None):
		'''
		Parse log between start_date and end_date, inclusive. Call function after each line if specified.
		In case of a single day, start_date and end_date should be equal.
		Method iterates through log files calling parseFile() on each. 
		
		Parameters:
			log_path: Path where log files are located. Typically "/var/log" or "/var/log/mail".
			log_file_base: Filename of last log. Typically "mail.log". 
			start_date: [YYYY-MM-DD or YYYYMMDD] First day to look for.
			end_date: [YYYY-MM-DD or YYYYMMDD] Last day to look for.
			fn_callback: Function which will be called on each line. LogLineData 
			             object is being passed as the argument.
		'''
		
		lst_files = self.__getLogFiles(log_path, log_file_base, start_date, end_date)
		for file_path in lst_files:
			fp = open(file_path)
			self.parseFile(fp, fn_callback)
			fp.close()
	
	def parseText(self, text, fn_callback = None):
		'''
		Parse text passed as the parameter line by line, call function after each line if specified.
		
		Parameters:
			text: String which should parsed.
			fn_callback: Function which will be called on each line. LogLineData 
			             object is being passed as the argument. 
		'''
		
		buf = io.StringIO(text)
		for line in buf.readlines():
			data = LogLineData(line)
			fn_callback(data)


if __name__ == "__main__":
	def my_callback(data):
		if data.data_type:
			print("[%s] Time: %s Host: %s QID: %s" % (data.data_type, data.log_time, data.hostname, data.qid))
	
	p = LogParser()
	log = '''
Mar 27 12:12:01 mailgate01 postfix/smtpd[23411]: C6CF080DED: client=budsexh1.cen.csin.cz[10.177.6.89]
Mar 27 12:12:01 mailgate01 postfix/cleanup[23576]: C6CF080DED: message-id=<6A17B10713935640830029058565883392D634B6@aspsexm2.cen.csin.cz>
Mar 27 12:12:01 mailgate01 postfix/qmgr[1758]: C6CF080DED: from=<tomas.brixi@s-itsolutions.cz>, size=3261, nrcpt=1 (queue active)
Mar 27 12:12:01 mailgate01 postfix/smtpd[19658]: DCB5080DE6: client=localhost[127.0.0.1]
Mar 27 12:12:01 mailgate01 postfix/cleanup[23576]: DCB5080DE6: message-id=<6A17B10713935640830029058565883392D634B6@aspsexm2.cen.csin.cz>
Mar 27 12:12:01 mailgate01 postfix/qmgr[1758]: DCB5080DE6: from=<tomas.brixi@s-itsolutions.cz>, size=3746, nrcpt=1 (queue active)
Mar 27 12:12:01 mailgate01 amavis[23621]: (23621-12) Passed CLEAN {RelayedOutbound}, LOCAL [10.177.6.89]:52226 [10.177.6.89] <tomas.brixi@s-itsolutions.cz> -> <tomas.brixi@gapps.s-itsolutions.cz>, Queue-ID: C52E180DEC, Message-ID: <6A17B10713935640830029058565883392D634B6@aspsexm2.cen.csin.cz>, mail_id: 4rMawbfpkunZ, Hits: -, size: 3263, queued_as: DCB5080DE6, 91 ms
Mar 27 12:12:01 mailgate01 postfix/smtpd[20030]: DF33580DEF: client=localhost[127.0.0.1]
Mar 27 12:12:01 mailgate01 postfix/cleanup[23632]: DF33580DEF: message-id=<6A17B10713935640830029058565883392D634B6@aspsexm2.cen.csin.cz>
Mar 27 12:12:01 mailgate01 postfix/smtp[23648]: C52E180DEC: to=<tomas.brixi@gapps.s-itsolutions.cz>, relay=127.0.0.1[127.0.0.1]:10024, delay=0.11, delays=0.01/0.01/0/0.09, dsn=2.0.0, status=sent (250 2.0.0 from MTA(smtp:[127.0.0.1]:10025): 250 2.0.0 Ok: queued as DCB5080DE6)
Mar 27 12:12:01 mailgate01 postfix/qmgr[1758]: DF33580DEF: from=<tomas.brixi@s-itsolutions.cz>, size=3743, nrcpt=1 (queue active)
Mar 27 12:12:01 mailgate01 amavis[23645]: (23645-02) Passed CLEAN {RelayedOutbound}, LOCAL [10.177.6.89]:52226 [10.177.6.89] <tomas.brixi@s-itsolutions.cz> -> <jiri.novak@gapps.s-itsolutions.cz>, Queue-ID: C6CF080DED, Message-ID: <6A17B10713935640830029058565883392D634B6@aspsexm2.cen.csin.cz>, mail_id: ne82w8C-9U9f, Hits: -, size: 3262, queued_as: DF33580DEF, 87 ms
Mar 27 12:12:01 mailgate01 postfix/smtp[23624]: C6CF080DED: to=<jiri.novak@gapps.s-itsolutions.cz>, relay=127.0.0.1[127.0.0.1]:10024, delay=0.11, delays=0/0.02/0/0.09, dsn=2.0.0, status=sent (250 2.0.0 from MTA(smtp:[127.0.0.1]:10025): 250 2.0.0 Ok: queued as DF33580DEF)
Mar 27 12:12:01 mailgate01 postfix/qmgr[1758]: C6CF080DED: removed
	'''
	print("LogParser.parseText():")
	p.parseText(log, my_callback)
	
	print()
	print("LogParser.parseFile():")
	fp = open("C:/users/sol60527/mail.log")
	p.parseFile(fp, my_callback)
	fp.close()
