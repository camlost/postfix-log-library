'''
Created on 3. 4. 2015

@author: camlost <jiri.novak@gmail.com>
'''

from enum import Enum
import re

class LogDataType(Enum):
	CLIENT    = 1
	MESSAGEID = 2
	FROM      = 3
	AMAVIS    = 4
	TO        = 5
	REMOVED   = 6
	
	
class LogLineData:
	'''
	classdocs
	'''

	__lst_pattern = [
		r"^([A-Za-z]{3}\s+\d+\s+[^\s]+)\s(\w+)\s([^[]+)\[(\d+)\]:\s([^:]+):\sclient=(.*)$",
		r"^([A-Za-z]{3}\s+\d+\s+[^\s]+)\s(\w+)\s([^[]+)\[(\d+)\]:\s([^:]+):\smessage-id=(.*)$",
		r"^([A-Za-z]{3}\s+\d+\s+[^\s]+)\s(\w+)\s([^[]+)\[(\d+)\]:\s([^:]+):\sfrom=<([^>]+)>,\ssize=(\d+),\snrcpt=(\d+)\s.*$",
		r"^([A-Za-z]{3}\s+\d+\s+[^\s]+)\s(\w+)\s([^[]+)\[(\d+)\]:[^)]+\)\s(.+)\s\{([^}]+)\}.*Queue-ID:\s(\w+),\sMessage-ID:\s<([^>]+)>.*queued_as:\s(\w+).*$",
		r"^([A-Za-z]{3}\s+\d+\s+[^\s]+)\s(\w+)\s([^[]+)\[(\d+)\]:\s(\w+):\sto=<([^>]+)>,\srelay=([^,]+),.*dsn=([^,]+),\sstatus=(.*)\s\((.*)$",
		r"^([A-Za-z]{3}\s+\d+\s+[^\s]+)\s(\w+)\s([^[]+)\[(\d+)\]:\s(\w+):\sremoved$"
		]
	__lst_regex = [re.compile(x) for x in __lst_pattern]


	def __init__(self, line = None):
		'''
		Constructor
		'''
		self.data_type = None   # Identifies content of the line (client, message-id, amavis log, etc.).
		self.log_time = None    # First item on each log line, log time.
		self.hostname = None    # Host on which was the line logged.
		self.qid = None         # Queue ID (= internal ID).
		self.client_host = None # Client SMTP server (host which sent the message to 'hostname').
		self.message_id = None  # Message-ID, a unique ID of a message.
		self.sender = None      # E-mail address of message sender.
		self.size = 0           # Message size in bytes.
		self.rcpt_count = 0     # Number of recipients.
		self.rcpt = None        # E-mail address of a recipient
		self.relay = None       # Relaying host (target SMTP server).
		self.dsn = None         # Delivery status notification code ("2.0.0", "5.5.0", etc.).
		self.status = None      # Status text ("sent", "dropped", etc.).
		self.response = None    # Response of relaying host ("queued as 12341234").
		self.finish_time = None # Time when message was removed from queue.
		
		if line:
			self.parseLine(line)
		
	def parseLine(self, line):
		'''
		Parse log line, set members accordingly.
		
		Parameters:
			line: String of text.
			
		Return:
			True if match found, False otherwise.
		'''
		
		result = False
		idx = 0
		for rxp in LogLineData.__lst_regex:
			match = rxp.search(line)
			if match:
				result = True
				self.log_time = match.group(1)
				self.hostname = match.group(2)
				# Process name = match.group(3)
				# Process ID = match.group(4)
				
				if idx == 3:   # amavis
					self.data_type = LogDataType.AMAVIS
					self.virus_found = match.group(5)
					self.virus_flags = match.group(6)
					self.source_qid = match.group(7)
					self.message_id = match.group(8)
					self.qid = match.group(9)
				else:
					self.qid = match.group(5)
					if idx == 0:     # client
						self.data_type = LogDataType.CLIENT
						self.client_host = match.group(6)
					elif idx == 1:   # message-id
						self.data_type = LogDataType.MESSAGEID
						self.message_id = match.group(6)
					elif idx == 2:   # from
						self.data_type = LogDataType.FROM
						self.sender = match.group(6)
						self.size = match.group(7)
						self.rcpt_count = match.group(8)
					elif idx == 4:   # to
						self.data_type = LogDataType.TO
						self.rcpt = match.group(6)
						self.relay = match.group(7)
						self.dsn = match.group(8)
						self.status = match.group(9)
						self.response = match.group(10)
					elif idx == 5:   # removed
						self.data_type = LogDataType.REMOVED
						self.finish_time = self.log_time
					else:
						result = False		
			idx += 1
		return result


if __name__ == "__main__":
	lst = [
		None,
		"", # Empty string
		"The knights that say NI!", # Non-matching string
		"Mar 27 12:12:01 mailgate01 postfix/smtpd[23411]: C6CF080DED: client=budsexh1.cen.csin.cz[10.177.6.89]", # Log line example
		"Mar 27 12:12:01 mailgate01 postfix/cleanup[23576]: C6CF080DED: message-id=<6A17B10713935640830029058565883392D634B6@aspsexm2.cen.csin.cz>", # Log line example
		"Mar 27 12:12:01 mailgate01 postfix/qmgr[1758]: C6CF080DED: from=<tomas.brixi@s-itsolutions.cz>, size=3261, nrcpt=1 (queue active)", # Log line example
		"Mar 27 12:12:01 mailgate01 postfix/qmgr[1758]: C6CF080DED: removed" # Log line example
		]
	for line in lst:
		data = LogLineData(line)
		if data.data_type:
			print("[%s] Time: %s QID: %s Hostname: %s" % (data.data_type, data.log_time, data.qid, data.hostname))